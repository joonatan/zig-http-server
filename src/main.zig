const std = @import("std");
const httpz = @import("httpz");
const c = @cImport({
    @cInclude("sqlite3.h");
});

const assert = std.debug.assert;

const MAX_USERS = 128;
const DEFAULT_PORT: u16 = 5882;

const User = struct {
    id: u32,
    name: []const u8,
    company: []const u8 = undefined,
};

const ExerciseReport = struct {
    id: u32,
    user_id: u32,
    date: u64,
    duration: u32,
};

const Store = struct {
    users: []User,
};

const Global = struct {
    hits: usize = 0,
    store: ?*Store = null,
    l: std.Thread.Mutex = .{},
    db: ?*c.sqlite3 = null,
};

// TODO extend this to allow overriding DATABASE_URL
// TODO load .env and modify the environment
pub fn loadEnv(allocator: std.mem.Allocator) !u16 {
    // TODO catch and return default if this fails?
    const env = try std.process.getEnvMap(allocator);
    const port = env.get("PORT") orelse {
        std.log.info("PORT environment variable not set: using default {d}", .{DEFAULT_PORT});
        return DEFAULT_PORT;
    };
    std.log.info("PORT={s}", .{port});
    return std.fmt.parseInt(u16, port, 10) catch {
        std.log.err("Invalid PORT environment variable: {s}", .{port});
        return DEFAULT_PORT;
    };
}

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    const port = try loadEnv(allocator);
    if (port == 0) {
        return;
    }

    const alloc_vars = try allocator.alloc(User, MAX_USERS);
    defer allocator.free(alloc_vars);
    for (alloc_vars, 0..) |_, i| {
        const id: u32 = @intCast(i);
        alloc_vars[i] = .{ .id = id + 1, .name = "User", .company = "Company" };
    }

    var db: ?*c.sqlite3 = null;
    const rc = c.sqlite3_open("test.db", &db);
    if (rc != c.SQLITE_OK) {
        std.debug.print("Can't open database: {s}\n", .{c.sqlite3_errmsg(db)});
        return;
    }
    assert(db != null);
    defer _ = c.sqlite3_close(db);

    var store = Store{
        .users = alloc_vars,
    };
    var global = Global{
        .hits = 0,
        .store = &store,
        .l = .{},
        .db = db,
    };
    var server = try httpz.ServerCtx(*Global, *Global).init(allocator, .{ .port = port }, &global);

    // overwrite the default notFound handler
    server.notFound(notFound);
    // overwrite the default error handler
    server.errorHandler(errorHandler);

    var router = server.router();

    // use get/post/put/head/patch/options/delete
    // you can also use "all" to attach to all methods
    router.get("/api/user/:id", getUser);
    router.get("/api/user/", listUsers);
    router.post("/api/user/", addUser);
    router.delete("/api/user/:id", deleteUser);
    router.get("/increment", increment);

    std.log.info("Listening on port {d}", .{port});
    // start the server in the current thread, blocking.
    return server.listen();
}

fn getUser(g: *Global, req: *httpz.Request, res: *httpz.Response) !void {
    std.log.info("Request: GET {s}", .{req.url.raw});

    const id = std.fmt.parseInt(u32, req.param("id").?, 10) catch {
        res.status = 400;
        try res.json(.{ .message = "Invalid user id" }, .{});
        return;
    };
    g.l.lock();
    defer g.l.unlock();
    assert(g.store != null);
    // TODO replace with sqlite3 query
    const users = g.store.?.users;
    for (users) |user| {
        if (user.id == id) {
            try res.json(user, .{});
            return;
        }
    }

    res.status = 404;
    try res.json(.{ .message = "User not found" }, .{});
}

fn listUsers(g: *Global, req: *httpz.Request, res: *httpz.Response) !void {
    std.log.info("Request: GET {s}", .{req.url.raw});
    g.l.lock();
    defer g.l.unlock();
    assert(g.store != null);
    var stmt: ?*c.sqlite3_stmt = null;
    defer _ = c.sqlite3_finalize(stmt);
    const q1 = "SELECT COUNT(*) FROM users";
    const rc1 = c.sqlite3_prepare_v2(g.db, q1, -1, &stmt, null);
    assert(rc1 == c.SQLITE_OK);
    assert(c.sqlite3_step(stmt) == c.SQLITE_ROW);
    const count = c.sqlite3_column_int(stmt, 0);
    const query = "SELECT * FROM users";
    const rc2 = c.sqlite3_prepare_v2(g.db, query, -1, &stmt, null);
    assert(rc2 == c.SQLITE_OK);
    assert(count >= 0);
    const size: usize = @intCast(count);
    const users: []User = try res.arena.alloc(User, size);
    defer res.arena.free(users);
    std.debug.print("size: {d} : c_size {d} \n", .{ size, count });
    for (0..size) |i| {
        assert(c.sqlite3_step(stmt) == c.SQLITE_ROW);
        // TODO does intCast do a mem cast or does it check for overflow?
        // we want mem cast since db id should always be unsigned
        const id = c.sqlite3_column_int(stmt, 0);
        // TODO write this into a helper fn (though there is probably a better way)
        const n = try res.arena.alloc(u8, @intCast(c.sqlite3_column_bytes(stmt, 1)));
        for (0..n.len) |j| {
            n[j] = c.sqlite3_column_text(stmt, 1)[j];
        }
        const comp = try res.arena.alloc(u8, @intCast(c.sqlite3_column_bytes(stmt, 2)));
        for (0..comp.len) |j| {
            comp[j] = c.sqlite3_column_text(stmt, 2)[j];
        }
        users[i] = .{
            .id = @intCast(id),
            .name = n,
            .company = comp,
        };
    }

    try res.json(&users, .{});
    // TODO this should be done in a defer
    for (users) |user| {
        res.arena.free(user.name);
        res.arena.free(user.company);
    }
}

const UserBody = struct {
    name: []const u8,
};

fn addUser(g: *Global, req: *httpz.Request, res: *httpz.Response) !void {
    std.log.info("Request: POST {s}", .{req.url.raw});

    const body = req.json(UserBody) catch {
        std.log.warn("Invalid request body: missing", .{});
        res.status = 400;
        try res.json(.{ .message = "Invalid request body" }, .{});
        return;
    };
    if (body == null) {
        std.log.warn("Invalid request body: null", .{});
        res.status = 400;
        try res.json(.{ .message = "Invalid request body" }, .{});
        return;
    }
    const name = body.?.name;
    assert(g.store != null);
    g.l.lock();
    defer g.l.unlock();
    const users = g.store.?.users;
    // TODO replace with sqlite3 query
    for (users, 0..) |user, i| {
        if (user.id == undefined) {
            // this is very problematic
            // but we should be using a database anyway
            const id: u32 = @intCast(users.len);
            users[i] = .{ .id = id + 1, .name = name };
            try res.json(user, .{});
            return;
        }
    }

    res.status = 400;
    try res.json(.{ .message = "User limit reached" }, .{});
}

fn deleteUser(g: *Global, req: *httpz.Request, res: *httpz.Response) !void {
    std.log.info("Request: DELETE {s}", .{req.url.raw});

    const id = std.fmt.parseInt(u32, req.param("id").?, 10) catch {
        res.status = 400;
        try res.json(.{ .message = "Invalid user id" }, .{});
        return;
    };
    g.l.lock();
    defer g.l.unlock();
    assert(g.store != null);
    const users = g.store.?.users;
    // TODO replace with sqlite3 query
    for (users, 0..) |user, i| {
        if (user.id == id) {
            // don't care about ordering but want to keep the slice compact
            // FIXME this doesn't work correctly
            // need to abstract it into a function and write tests
            var last_valid: usize = users.len - 1;
            for (users[i..], i..) |u, j| {
                if (j == users.len - 1) {
                    break;
                }
                if (u.id == undefined) {
                    last_valid = j;
                    break;
                }
            }
            users[i] = users[last_valid];
            users[users.len - 1] = .{ .id = undefined, .name = undefined };
            return;
        }
    }

    res.status = 404;
    try res.json(.{ .message = "User not found" }, .{});
}

fn notFound(_: *Global, req: *httpz.Request, res: *httpz.Response) !void {
    std.log.info("Not Found {s}", .{req.url.raw});
    res.status = 404;
    try res.json(.{ .message = "Not Found" }, .{});
}

// note that the error handler return `void` and not `!void`
fn errorHandler(_: *Global, req: *httpz.Request, res: *httpz.Response, err: anyerror) void {
    res.status = 500;
    res.body = "Internal Server Error";
    std.log.warn("httpz: unhandled exception for request: {s}\nErr: {}", .{ req.url.raw, err });
}

fn increment(global: *Global, _: *httpz.Request, res: *httpz.Response) !void {
    global.l.lock();
    const hits = global.hits + 1;
    global.hits = hits;
    global.l.unlock();

    // or, more verbosse: httpz.ContentType.TEXT
    res.content_type = .TEXT;
    res.body = try std.fmt.allocPrint(res.arena, "{d} hits", .{hits});
}
