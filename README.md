# README

Zig web server example with sqlite3 database.

Just a personal learning project for zig.

Includes:
- http server using a package (not std)
- sqlite3 database (using C interface)

## Build

```sh
zig build
```

## Run

```sh
sqlite3 db.sqlite3 < schema.sql
sqlite3 db.sqlite3 < seed.sql
zig build run
```

## TODO

- [ ] Finish the migration to database
- [ ] Add more complex database schema
- [ ] Add route to retrieve a monthly report
- [ ] Add authentication (admin only)
- [ ] Add tests (unit and integration)
- [ ] Add CI scripts
- [ ] Performance tests using k6
